package com.web.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Language {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",nullable=false,updatable=false)
	private Long id;
	

	@Column(nullable = false)
	private String code;

	public Language() {

		// TODO Auto-generated constructor stub
	}

	public Language(String code) {
		// TODO Auto-generated constructor stub
		this.code = code;
	}

	
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = { CascadeType.MERGE },
    mappedBy = "languages")

	private List<Developer> developers ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonIgnore
	public List<Developer> getDevelopers() {
		return developers;
	}

	public void setDevelopers(List<Developer> developers) {
		this.developers = developers;
	}

	

}
