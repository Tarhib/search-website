package com.web.demo.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Developer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, updatable = false)
	private Long id;

	@Column(nullable = false)
	private String email;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@JoinTable(name = "developer_languages", joinColumns = {
			@JoinColumn(name = "developer_id") }, inverseJoinColumns = { @JoinColumn(name = "language_id") })

	private List<Language> languages;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@JoinTable(name = "developer_programminglanguages", joinColumns = {
			@JoinColumn(name = "developer_id") }, inverseJoinColumns = { @JoinColumn(name = "programminglanguage_id") })
	private List<ProgrammingLanguage> programmingLanguages;

	public Developer() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Developer(String email) {
		super();
		this.email = email;

	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<ProgrammingLanguage> getProgrammingLanguages() {
		return programmingLanguages;
	}

	public void setProgrammingLanguages(List<ProgrammingLanguage> programmingLanguages) {
		this.programmingLanguages = programmingLanguages;
	}

	@Override
	public String toString() {
		return "Developer [id=" + id + ", email=" + email + ", languages=" + languages + ", programmingLanguages="
				+ programmingLanguages + "]";
	}

	
}
