package com.web.demo.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.web.demo.model.Developer;
import com.web.demo.model.Language;
import com.web.demo.model.ProgrammingLanguage;
import com.web.demo.repository.DeveloperRepository;
import com.web.demo.repository.LanguageRepository;
import com.web.demo.repository.ProgrammingLanguageRepository;

@Component
public class DataLoader implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(DataLoader.class);

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private ProgrammingLanguageRepository programmingLanguageRepository;

	@Autowired
	DeveloperRepository developerRepository;

	@Override
	public void run(String... strings) throws Exception {
		logger.info("Loading data...");
		List<Developer> initialDevelopers = new ArrayList<>();
		Language bangla = new Language("bn");
		bangla.setDevelopers(initialDevelopers);
		languageRepository.save(bangla);


		Language english = new Language("en");
		english.setDevelopers(initialDevelopers);
		languageRepository.save(english);

		Language japanese = new Language("ja");
		japanese.setDevelopers(initialDevelopers);
		languageRepository.save(japanese);

		Language chinese = new Language("zh-CN");
		chinese.setDevelopers(initialDevelopers);
		languageRepository.save(chinese);

		Language korean = new Language("ko");
		korean.setDevelopers(initialDevelopers);
		languageRepository.save(korean);

		Language russian = new Language("ru");
		russian.setDevelopers(initialDevelopers);
		languageRepository.save(russian);

		Language french = new Language("fr");
		french.setDevelopers(initialDevelopers);
		languageRepository.save(french);

		Language hindi = new Language("hi");
		hindi.setDevelopers(initialDevelopers);
		languageRepository.save(hindi);

		List<Language> languages = new ArrayList<>();
		languages.add(french);
		languages.add(korean);
		languages.add(japanese);
		languages.add(bangla);
		languages.add(hindi);
		languages.add(russian);
		languages.add(chinese);


		ProgrammingLanguage java = new ProgrammingLanguage("java");
		java.setDevelopers(initialDevelopers);	
		programmingLanguageRepository.save(java);
		
		ProgrammingLanguage ruby = new ProgrammingLanguage("ruby");
		ruby.setDevelopers(initialDevelopers);		
		programmingLanguageRepository.save(ruby);
		
		ProgrammingLanguage php = new ProgrammingLanguage("php");
		php.setDevelopers(initialDevelopers);
		programmingLanguageRepository.save(php);

		ProgrammingLanguage python = new ProgrammingLanguage("python");
		python.setDevelopers(initialDevelopers);
		programmingLanguageRepository.save(python);
		ProgrammingLanguage javascript = new ProgrammingLanguage("javascript");
		javascript.setDevelopers(initialDevelopers);
		programmingLanguageRepository.save(javascript);

		

		ProgrammingLanguage kotlin = new ProgrammingLanguage("kotlin");
		kotlin.setDevelopers(initialDevelopers);
		programmingLanguageRepository.save(kotlin);
		
		ProgrammingLanguage c = new ProgrammingLanguage("c");
		c.setDevelopers(initialDevelopers);
		programmingLanguageRepository.save(c);
		

		List<ProgrammingLanguage> programmingLanguages = new ArrayList<>();
		
		programmingLanguages.add(ruby);
		programmingLanguages.add(java);
		programmingLanguages.add(php);
		programmingLanguages.add(javascript);
		programmingLanguages.add(python);
		
		programmingLanguages.add(kotlin);
		programmingLanguages.add(c);
		
		
		for(int i=0;i<20;i++)
		{
			Developer developer = new Developer(generateRandomWords());
			Random random = new Random();
			int randomNumber = random.nextInt(languages.size());
			
			
			List<Language>linkedLanguages = new ArrayList<>();
			linkedLanguages = languages.subList(0, randomNumber);
			developer.setLanguages(linkedLanguages);
			
			
			
			for (Language language : linkedLanguages) {
				language.getDevelopers().add(developer);
			}
			 randomNumber = random.nextInt(programmingLanguages.size());
			
			List<ProgrammingLanguage>linkedProgrammingLanguages = new ArrayList<>();
			linkedProgrammingLanguages = programmingLanguages.subList(0, randomNumber);
			developer.setProgrammingLanguages(linkedProgrammingLanguages);
			
			
			for (ProgrammingLanguage programmingLanguage : linkedProgrammingLanguages) {
				programmingLanguage.getDevelopers().add(developer);
			}
			developerRepository.save(developer);
		}
		
		
		
		
		
		

		
		
		

	}

	public static String generateRandomWords() {
		Random random = new Random();
		char[] word = new char[4];
		String prefix = null, postfix = null;
		for (int j = 0; j < 4; j++) {
			word[j] = (char) ('a' + random.nextInt(26));
			prefix = new String(word);

		}
		for (int j = 0; j < 4; j++) {
			word[j] = (char) ('a' + random.nextInt(26));
			postfix = new String(word);

		}
		String email = prefix + "." + postfix + "@example.com";
		return email;
	}

}