package com.web.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.demo.model.Interview;

public interface InterviewRepository extends JpaRepository<Interview, Long> {

}
