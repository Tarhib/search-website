package com.web.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.demo.model.Developer;
import com.web.demo.model.Language;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Long> {

	/* public Iterable<Bob> findAllByTomsContains(List<Tom> toms); */

	List<Developer> findByProgrammingLanguagesNameInAndLanguagesCode(List<String> programmingLanguagesName,
			String language);

	// List<Developer>
	// findAllByProgrammingLanguagesNameContains(List<String>programminglanguage);

	List<Developer> findByLanguagesCodeAndProgrammingLanguagesName(String language, String programminglanguage);
	// 1 1 0

	// findByAddressCity findByInventoryIdIn(List<Long> inventoryIdList)
	List<Developer> findByProgrammingLanguagesName(String programminglanguage);

	// 1 0 1

	// 1 0 0

}
