package com.web.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.demo.model.Language;

public interface LanguageRepository extends JpaRepository<Language, Long> {
	public Language findByCode(String code);
}
