package com.web.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.demo.model.Language;
import com.web.demo.model.ProgrammingLanguage;

public interface ProgrammingLanguageRepository extends JpaRepository<ProgrammingLanguage,Long> {
	
	public ProgrammingLanguage findByName(String name);
}
