package com.web.demo.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.web.demo.model.Developer;
import com.web.demo.repository.DeveloperRepository;

@RestController
@RequestMapping("/api/rest/developer")
public class DeveloperRestController {

	@Autowired
	private DeveloperRepository developerRepository;

	// Get Developers as list
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Developer> getAllDevelopers() throws Exception {
		return developerRepository.findAll();
	}

	// Create Developer
	@PostMapping("/add")
	public Developer createDeveloper(@RequestBody Developer developer) {
		Developer savedDeveloper = developerRepository.save(developer);

		return savedDeveloper;

	}

	// Edit Developer
	@PutMapping("/{id}")
	public Developer updateDeveloper(@RequestBody Developer developer, @PathVariable long id) {

		developer.setId(id);

		return developerRepository.save(developer);

	}

	@DeleteMapping("/{id}")
	public String  deleteStudent(@PathVariable long id) {
		developerRepository.deleteById(id);
		String success = "Deleted Successfully";
		return success;
	}

}
