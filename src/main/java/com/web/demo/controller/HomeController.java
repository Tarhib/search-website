package com.web.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.web.demo.model.Developer;
import com.web.demo.repository.DeveloperRepository;

@Controller
public class HomeController {

	@Autowired
	DeveloperRepository developerRepository;
	
	@GetMapping("/home")
	public String  getAllDevelopers(Model model)
	{
		
		List<Developer>developerList = developerRepository.findAll();
		System.out.println(developerList.toString());
		model.addAttribute("developerList", developerList);
		
		return "view";
	}
	

}
