package com.web.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.web.demo.model.Developer;
import com.web.demo.model.Language;
import com.web.demo.model.ProgrammingLanguage;
import com.web.demo.repository.DeveloperRepository;
import com.web.demo.repository.LanguageRepository;
import com.web.demo.repository.ProgrammingLanguageRepository;

@RestController
@RequestMapping("/api")
public class DeveloperController {

	@Autowired
	DeveloperRepository developerRepository;

	@Autowired
	LanguageRepository languageRepository;
	@Autowired
	ProgrammingLanguageRepository programmingLanguageRepository;

	@GetMapping("/developer/list")
	public List<Developer> getAllDeveloper() {
		List<Developer> developerList = developerRepository.findAll();
		return developerList;
	}

	@GetMapping("/language/list")
	public List<Language> getAllLanguages() {
		List<Language> languageList = languageRepository.findAll();
		return languageList;
	}

	@GetMapping("/programming/language/list")
	public List<ProgrammingLanguage> getAllProgrammingLanguages() {
		List<ProgrammingLanguage> programmingLanguageList = programmingLanguageRepository.findAll();
		return programmingLanguageList;
	}

	@PostMapping("/developer/search")

	public ModelAndView showSerachResult(@RequestParam("languagename") String languagename,
			@RequestParam("secondlanguage") String secondlanguage,
			@RequestParam("proglanguagename") String proglanguagename) {

		System.out.println(proglanguagename + " " + languagename + " " + secondlanguage);
		ModelAndView modelAndView = new ModelAndView("view :: developer-list");

		List<Developer> developerList = new ArrayList<>();

		if (languagename.isEmpty()) {
			System.out.println("Hello for one call");
			developerList = developerRepository.findByProgrammingLanguagesName(proglanguagename);
		} else if (secondlanguage.isEmpty()) {
			System.out.println("Hello for two call");
			developerList = developerRepository.findByLanguagesCodeAndProgrammingLanguagesName(languagename,
					proglanguagename);
		} else if (languagename.isEmpty() && proglanguagename.isEmpty() && secondlanguage.isEmpty()) {
			System.out.println("Hello for no call");
			developerList = developerRepository.findAll();
		} else {
			System.out.println("Hello for three call");
			List<String> languageCodeList = new ArrayList<>();
			languageCodeList.add(proglanguagename);
			languageCodeList.add(secondlanguage);
			System.out.println(languageCodeList.toString());

			//developerList = developerRepository.findByProgrammingLanguagesNameAndProgrammingLanguagesNameAndLanguagesCode(proglanguagename,secondlanguage,languagename);
			developerList = developerRepository.findByProgrammingLanguagesNameInAndLanguagesCode(languageCodeList,languagename);

		}

		System.out.println(developerList.toString());
		modelAndView.addObject("developerList", developerList);
		return modelAndView;

	}

}
